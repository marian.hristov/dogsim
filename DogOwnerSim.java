public class DogOwnerSim {
    public static void main(String[] args) {
        Dog dog = new Dog("Gromit");
        Human human = new Human("Wallace");

        for (int i = 0; i < 5; i++) {
            while (dog.takeNap() && human.doWork()) {
            }
            while (dog.playWith(human) && human.feed(dog)) {
            }
        }
    }

}

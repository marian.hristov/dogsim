import javax.swing.border.Border;

public class Human {
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String name) {
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean doWork() {
        if (this.hunger > 50) {
            System.out.println("Human was too hungry");
            return false;
        }
        if (this.boredom > 50) {
            System.out.println("Human was too bored");
            return false;
        }
        this.money += 20;
        this.hunger += 5;
        this.boredom += 10;
        System.out.println("Human worked succesfully");
        return true;
    }

    public void play() {
        if (this.boredom < 30) {
            this.boredom -= this.boredom;
        } else {
            this.boredom -= 30;
            this.hunger += 5;
            System.out.println("Human played with the dog.");
        }
    }

    public void eat() {
        if (this.hunger <= 30) {
            this.hunger = 0;
        } else {
            this.hunger -= 30;
        }
        System.out.println("Human ate hot dog");
    }

    public boolean feed(Dog dog) {
        if (this.money > 50) {
            this.money -= 50;
            System.out.println("Hooman bought food for them and fiend doggo");
            this.eat();
            dog.eat();
            return true;
        } else {
            System.out.println("Hooman doesn't have enough money to feed doggo");
            return false;
        }
    }
}
public class Dog {
    private String name;
    private int energy;
    private int hunger;
    private int boredom;

    public Dog(String name) {
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap() {
        boolean canNap = true;
        if (this.boredom > 50) {
            System.out.println("The dog is too bored and can't sleep.");
            canNap = false;
        } else if (this.hunger > 50) {
            System.out.println("The dog is too hungry and can't sleep.");
            canNap = false;
        } else if (this.boredom <= 50 && this.hunger <= 50) {
            this.energy += 20;
            this.boredom += 10;
            this.hunger += 5;
            System.out.println("The dog napped.");
        }
        return canNap;
    }

    public void play() {
        if (this.boredom < 30) {
            this.boredom -= this.boredom;
        } else {
            this.boredom -= 30;
            this.hunger += 5;
            System.out.println("Dog played with the human.");
        }
    }

    public boolean playWith(Human human) {
        if (this.energy < 50) {
            System.out.println("The dog doesn't have enough energy to play.");
            return false;
        } else {
            this.energy -= 50;
            this.play();
            human.play();
            return true;
        }
    }

    public void eat() {
        if (this.hunger <= 30) {
            this.hunger = 0;
        } else {
            this.hunger -= 30;
        }
        System.out.println("Dog ate good soup");
    }
}